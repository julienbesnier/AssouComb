package Player;

import java.awt.Color;

/**
 *     @inv
 *     <pre>
 *         nbC >= 0
 *         nbT >= 0
 *         reset => nbC == 0
 *             && nbT == 0
 * </pre>
 * 
 *    @cons
 *        <pre>
 *            $ARGS$ int shotNB
 *            $POST$ appel à super(shotNB)
 *                nbC = 0
 *                nbT = 0
 *        </pre>
 */

public class Majority extends StdPlayer {
    
    // ATTRIBUTS
    // nombre de coopérations de l'adversaire
    private int nbC;
    // nombre de trahisons de l'adversaire
    private int nbT;

    // CONTRUCTEUR
    public Majority(int shotNB) {
        super(shotNB);
        this.nbC = 0;
        this.nbT = 0;
    }
    
    // REQUETES 
    public Color getColor() {
        return PlayerColor.MAJORITY.getColor();
    }

    // COMMANDE
    public void strategicShot() {
        if (getShotNb() == 1) {
            cooperate();
        } else {
            // mise à jour du nombre de coopération(s)/trahison(s) 
            //   de l'adversaire
            if (getEnemy().getShots(getShotNb() - 1) == COOPERATION) {
                nbC = nbC + 1;
            } else {
                nbT = nbT + 1;
            }
            if (nbC < nbT) {
                betray();
            } else {
                cooperate();
            }
        }
        
    }
    
    public String getName() {
        return "Majorité";
    }

    @Override
    public void reset() {
        super.reset();
        this.nbC = 0;
        this.nbT = 0;
    }
}
