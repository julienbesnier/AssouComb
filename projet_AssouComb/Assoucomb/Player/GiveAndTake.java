package Player;

import java.awt.Color;

/**
 *    @cons
 *        <pre>
 *            $ARGS$ int shotNB
 *            $POST$ appel à super(shotNB)
 *        </pre>
 */

public class GiveAndTake extends StdPlayer {
    
    // CONSTRUCTEUR
    public GiveAndTake(int shotNB) {
        super(shotNB);
    }
    
    // REQUETES 
    public Color getColor() {
        return PlayerColor.GIVEANDTAKE.getColor();
    }

    // COMMANDE
    public void strategicShot() {
        if (getShotNb() == 0) {
            cooperate();
        } else {
            int tab[] = getEnemy().getShots();
            if (tab[getShotNb() - 1] == COOPERATION) {
                cooperate();
            } else {
                betray();
            }
        }
    }
    
    public String getName() {
        return "Donnant-donnant";
    }
}
