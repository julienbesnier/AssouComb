package Player;

import java.awt.Color;

/**
 *     @cons
 *        <pre>
 *            $ARGS$ int shotNB
 *            $POST$ appel à super(shotNB)
 *        </pre>
 */

public class GentlePeriodic extends StdPlayer {
    
    // CONSTRUCTEUR
    
    public GentlePeriodic(int shotNB) {
        super(shotNB);
    }

    // REQUETES 
    public Color getColor() {
        return PlayerColor.GENTLEPERIODIC.getColor();
    }
    
    // COMMANDE
    
    public void strategicShot() {
        if (this.getShotNb() % 3 == 2) {
            betray();
        } else {
            cooperate();
        }
    }
    
    public String getName() {
        return "Périodique gentille";
    }
}
