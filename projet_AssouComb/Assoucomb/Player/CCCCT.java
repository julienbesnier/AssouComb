package Player;

import java.awt.Color;

/**
 *     @cons
 *        <pre>
 *            $ARGS$ int shotNB
 *            $POST$ appel à super(shotNB)
 *        </pre>
 */

public class CCCCT extends StdPlayer {
    
    // CONTRUCTEUR
    
    public CCCCT(int shotNB) {
        super(shotNB);
    }

    // REQUETES 
    public Color getColor() {
        return PlayerColor.CCCCT.getColor();
    }
    
    // COMMANDE
    
    public void strategicShot() {
        if (this.getShotNb() % 5 == 4) {
            betray();
        } else {
            cooperate();
        }
    }
    
    public String getName() {
        return "CCCCT";
    }
}
