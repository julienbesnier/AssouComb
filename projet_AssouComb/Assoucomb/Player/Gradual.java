package Player;

import java.awt.Color;

/**
 *     @inv
 *     <pre>
 *         cptT >= 0
 *         nbTInst >= 0
 *         nbTT >= 0
 *         nb >= 0
 *         reset => cptT == 0
 *             && nbTInst == 0
 *             && nbTT == 0
 *             && nb == 0
 * </pre>
 * 
 *    @cons
 *        <pre>
 *            $ARGS$ int shotNB
 *            $POST$ appel à super(shotNB)
 *                cptT >= 0
 *                 nbTInst >= 0
 *                 nbTT >= 0
 *                 nb >= 0
 *        </pre>
 */

public class Gradual extends StdPlayer {
    
    // ATTRIBUTS 
    //compteur de trahisons de l'adversaire
    private int cptT;
    
    //nombre de coups trahis par le joeur instantané
    private int nbTInst;
    
    //nombre de coups trahis par le joeur total
    private int nbTT;
    
    //nombre de fois à coopérer après les trahisons
    private int nb;
    
    // CONTRUCTEUR
    
    public Gradual(int shotNB) {
        super(shotNB);
        this.cptT = 0;
        this.nbTInst = 0;
        this.nbTT = 0;
        this.nb = 0;
    }
    
    // REQUETES 
    public Color getColor() {
        return PlayerColor.GRADUAL.getColor();
    }

    // COMMANDE
    
    public void strategicShot() {
        if (cptT == 0 || nb > 0) {
            if (nb != 0) {
                nb = 0;
            }
            cooperate();
            nbTInst = 0;
        } else if (nbTT < cptT) {
            betray();
            nbTT = nbTT + 1;
            nbTInst = nbTInst + 1;
        } else if (nbTInst < cptT) {
            betray();
            nbTT = nbTT + 1;
            nbTInst = nbTInst + 1;
        } else {
            if (getShots(getShotNb()) == BETRAYAL) {
                cooperate();
                nbTInst = nbTInst + 1;
            }
        }
    }
    
    public String getName() {
        return "Graduelle";
    }

    @Override
    public void reset() {
        super.reset();
        this.cptT = 0;
        this.nbTInst = 0;
        this.nbTT = 0;
        this.nb = 0;
    }
}
