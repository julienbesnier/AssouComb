package Player;

import java.awt.Color;

public abstract class StdPlayer implements Player, Cloneable {
    
    // ATTRIBUTS
    private int[] tab;
    private int shotNb;
    private StdPlayer enemy;
    private int x;
    private int y;
    
    
    // CONSTRUCTEUR
    public StdPlayer(int n) {
        this.tab = new int[n];
        this.shotNb = 1;
        this.enemy = null;
        this.x = 0;
        this.y = 0;
    }
    
    
    // REQUÊTES
    public int[] getShots() {
        return tab;
    }

    public int getShotNb() {
        return shotNb;
    }

    public int getLastShot() {
        return tab[shotNb - 1];
    }

    public int getShots(int i) {
        return tab[i - 1];
    }
    
    public StdPlayer getEnemy() {
        return enemy;
    }

    public abstract Color getColor();
    
    public int getCooX() {
        return x;
    }
    
    public int getCooY() {
        return y;
    }
    
    
    // COMMANDES
    public void cooperate() {
        tab[getShotNb() - 1] = COOPERATION; // cooperer = 0
        shotNb = shotNb + 1;
    }
    
    public void betray() {
        tab[getShotNb() - 1] = BETRAYAL; // trahir = 1
        shotNb = shotNb + 1;
    }

    public void setEnemy(StdPlayer enmy) {
        if (this.equals(enmy)) {
            throw new AssertionError("Le joueur joue avec lui même...");
        }
        this.enemy = enmy;
    }
    
    public void setCoo(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    public void reset() {
        final int longueur = tab.length;
        this.tab = new int[longueur];
        this.shotNb = 1;    
        this.enemy = null;
    }
    
    public void setShotsNb(int nb) {
        if (nb <= 0) {
            throw new AssertionError();
        }
        shotNb = 1;
        tab = new int[nb];
    }
    
    // Clonage comme vu en MPOO2
    @Override
    public Object clone() {
        StdPlayer clone = null;
        try {
            clone = (StdPlayer) super.clone();
        } catch(CloneNotSupportedException e) {
            throw new InternalError("Ne devrait jamais arriver");
        }
        return clone;
    }
    
    
    // METHODES ABSTRAITES 
    public abstract void strategicShot();
    
    public abstract String getName();
}
