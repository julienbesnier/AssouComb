package Player;

import java.awt.Color;

/**
 *    @cons
 *        <pre>
 *            $ARGS$ int shotNB
 *            $POST$ appel à super(shotNB)
 *        </pre>
 */

public class ViciousPeriodic extends StdPlayer {
    // CONTRUCTEUR

    public ViciousPeriodic(int shotNB) {
        super(shotNB);
    }
    
    // REQUETES 
    public Color getColor() {
        return PlayerColor.VICIOUSPERIODIC.getColor();
    }

    // COMMANDE
    
    public void strategicShot() {
        if (this.getShotNb() % 3 == 0) {
            cooperate();
        } else {
            betray();
        }
    }
    
    public String getName() {
        return "Périodique méchante";
    }
}
