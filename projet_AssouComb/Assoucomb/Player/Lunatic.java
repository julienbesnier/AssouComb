package Player;

import java.awt.Color;

/**
 * @cons
 *        <pre>
 *            $ARGS$ int shotNB
 *            $POST$ appel à super(shotNB)
 *        </pre>
 */

public class Lunatic extends StdPlayer {
    // CONSTRUCTEUR
    public Lunatic(int shotNB) {
        super(shotNB);
    }

    // REQUETES 
    public Color getColor() {
        return PlayerColor.LUNATIC.getColor();
    }
    
    // COMMANDE
    public void strategicShot() {
        if (Math.random() < 0.5) {
            cooperate();
        } else {
            betray();
        }
    }
    
    public String getName() {
        return "Lunatique";
    }
}
