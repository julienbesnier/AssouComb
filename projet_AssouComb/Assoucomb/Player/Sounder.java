package Player;

import java.awt.Color;
/**
 *    @cons
 *        <pre>
 *            $ARGS$ int shotNB
 *            $POST$ appel à super(shotNB)
 *        </pre>
 */
public class Sounder extends StdPlayer {
    // CONTRUCTEUR
    public Sounder(int shotNB) {
        super(shotNB);
    }
    
    // REQUETES 
    public Color getColor() {
        return PlayerColor.SOUNDER.getColor();
    }

    // COMMANDE
    public void strategicShot() {
        if (getShotNb() == 0) {
            betray();
        } else if (getShotNb() == 1 || getShotNb() == 2) {
            cooperate();
        } else if (getEnemy().getShots(2) == COOPERATION &&
                getEnemy().getShots(3) == COOPERATION) {
            betray();
        } else if (getShotNb() == 3) {
            cooperate();
        } else if (getEnemy().getShots(getShotNb()) == COOPERATION) {
            cooperate();
        } else {
            betray();
        }
    }
    
    public String getName() {
        return "Sondeur";
    }
}
