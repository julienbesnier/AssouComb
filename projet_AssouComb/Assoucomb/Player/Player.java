package Player;

import java.awt.Color;
/**
 *    Player : représente le joueur qui dispose d'une stratégie donnée
 *    
 *    @inv
 *        <pre>
 *        getShoots() == tab
 *        getShootNb() == numéro du coup à jouer
 *        getLastShoot() == dernier coup
 *        getShots(i) == ième coup
 *        getEnemy() == l'ennemi courant du joueur
 *        getColor() == la couleur de l'ennemi
 *        getCooX() >= 0
 *        getCooY() >= 0
 *        cooperate => tab[getShootNb()] == COOPERATE
 *        betray => tab[getShootNb()] == BETRAYAL
 *        setEnemy(e) => getEnemy() == e
 *        setCoo(x, y) => getCooX() == x && getCooY() == y
 *        reset() => getShotNb() == 0
 *                    && tab = new int[old tab.length]
 *                    && enemy == null
 *        setShotsNb(nb) == nouvelle partie de nb coups
 *        </pre>
 *    @cons
 *        <pre>
 *            $ARGS$ int n
 *            $PRE$ no pre-condition
 *            $POST$ le tableau tab est un tableau de longueur n.
 *                Le numéro du prochain coup est 1. L'ennemi vaut null.
 *                Les coordonnées du joueur sont 0 en x et 0 en y.
 *        </pre>
 */

public interface Player extends Cloneable {
    // CONSTANTES
    public final int COOPERATION = 0;
    public final int BETRAYAL = 1;
    
    
    // REQUETES
    /**
     * Retourne le tableau des coups
     */
    int[] getShots();
    
    /**
     * Retourne le numéro du coup à jouer
     */
    int getShotNb();
    
    /**
     * Retourne le dernier coup (coop (0) ou trahir (1))
     */
    int getLastShot();
    
    /**
     * Retourne le ième coup (tab[i-1])
     */
    int getShots(int i);
    
    /**
     * Retourne l'adversaire du joueur
     */
    StdPlayer getEnemy();

    /**
     * Retourne la couleur du joueur (la couleur est propre à la stratégie).
     */
    Color getColor();
    
    /**
     * Renvoie la coordonnée en x du joueur
     */
    int getCooX();
    
    /**
     * Renvoie la coordonnée en y du joueur
     */
    int getCooY();
    
    
    // COMMANDES 
    /**
     * cooperer
     */
    void cooperate();
    
    /**
     * trahir
     */
    void betray();
    
    /**
     * Choisit l'adversaire de ce joueur (this).
     * 
     * @pre: !this.equals(enmy)
     */
    void setEnemy(StdPlayer enmy);

    /**
     * Enregistre les coordonnées du joueur
     */
    void setCoo(int x, int y);
    
    /**
     * Remet à 0 la table du nombre de coup
     * 
     * @post
     *     shotNb = 1;
     *     isTabEmpty();
     *  getEnemy() == null;
     */
    void reset();
    
    /**
     * Fixe la taille du tableau de coups
     * 
     * @pre
     *     nb > 0
     * @post
     *     shotNb = 1
     *     isTabEmpty()
     */
    void setShotsNb(int nb);
    
    
    // ABSTRACT
    /**
     * Stratégie du joueur. Méthode abstraite
     */
    void strategicShot();
    
    /**
     * Retourne le nom de la stratégie du joueur
     */
    String getName();
}
