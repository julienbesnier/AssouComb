package Player;

import java.awt.Color;

/**
 *     @cons
 *        <pre>
 *            $ARGS$ int shotNB
 *            $POST$ appel à super(shotNB)
 *        </pre>
 */

public class Binary extends StdPlayer{

    // CONTRUCTEUR
    
    public Binary(int shotNB) {
        super(shotNB);
    }

    // REQUETES 
    public Color getColor() {
        return PlayerColor.BINARY.getColor();
    }
    
    // COMMANDE
    public void strategicShot() {
        if (this.getShotNb() % 2 == 0) {
            cooperate();
        } else {
            betray();
        }
    }
    
    public String getName() {
        return "Binaire";
    }
}
