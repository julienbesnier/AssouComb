package Player;

import java.awt.Color;

public enum PlayerColor {
    LUNATIC(new Color(255, 128, 0)), GIVEANDTAKE(Color.BLUE), BINARY(Color.GRAY),
    GENTLEPERIODIC(Color.GREEN), VICIOUSPERIODIC(Color.RED), CCCCT(Color.PINK),
    SOUNDER(Color.CYAN), MAJORITY(Color.YELLOW), GRADUAL(Color.MAGENTA);
    
    private Color color;
    
    private PlayerColor(Color c) {
        color = c;
    }
    
    public Color getColor() {
    	return color;
    }
}
