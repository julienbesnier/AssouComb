package Board;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

public class Graph {
    
    JFreeChart lineChart;
    private DefaultCategoryDataset dataset;
    private ChartPanel panel;

    public Graph(String chartTitle, String x, String y) {
        dataset = createDataset();
        lineChart = ChartFactory.createLineChart(
            chartTitle,
            x, y,
            dataset,
            PlotOrientation.VERTICAL,
            true,true,false);
             
         panel = new ChartPanel(lineChart);
         panel.setPreferredSize(new java.awt.Dimension( 560 , 367 ));
       }

       private DefaultCategoryDataset createDataset() {
          DefaultCategoryDataset dataset = new DefaultCategoryDataset();
          return dataset;
       }
       
       public void changeData(int value, Comparable rowKey, Comparable columnKey) {
           dataset.addValue(value, rowKey, columnKey);
       }

       public ChartPanel getPanel() {
           return panel;
       }
       
       public JFreeChart getLineChart() {
           return lineChart;
       }
       
       public DefaultCategoryDataset getDataset() {
           return dataset;
       }
}
