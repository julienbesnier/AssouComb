package Board;

import java.util.List;

import javax.swing.JPanel;

import Player.StdPlayer;

/**
 * Board : Affiche le plateau après avoir placé les joueurs aléatoirement 
 *         sur le plateau au début de la partie, puis en les replaçant 
 *         selon le nombre de points gagnés par les joueurs au cours de 
 *         chaque partie.
 * @inv
 *         board(x, y) != null
 *         getBoard() != null
 *         2 <= getHeight()
 *         2 <= getWidth()
 *         for nbP1 := 0, nbP2 := 0
 *         for tabP1 := P1.getShots(), tabP2 := P2.getShots()
 *             forall i:[0..nbCoups[ :
 *                 if tabP1[i] == COOPERATION && tabP2[i] == COOPERATION
 *                    nbP1 += 1;
 *                    nbP2 += 1;
 *                if tabP1[i] == COOPERATION && tabP2[i] == BETRAYAL) {
 *                    nbP1 += 5;
 *                if tabP1[i] == BETRAYAL && tabP2[i] == COOPERATION) {
 *                    nbP2 += 5;
 *                else
 *                    nbP1 += 3;
 *                    nbP2 += 3;
 *            
 *                     
 *          
 *      
 *  @cons
 *        $DESC$ Plateau pré-paramétré dont la hauteur et la largeur sont 
 *               de 8 et le nombre de coups par partie entre deux joueurs 
 *               est de 20.
 *        $ARG$ -
 * 
 * @cons
 *        $DESC$ 
 *             Plateau dont la hauteur <code>x</code> et la largeur 
 *             <code>y</code> ainsi que le nombre de coups par partie
 *             entre deux joueurs <code>nbCoups</code> sont définis 
 *             par l'utilisateur.
 *        $ARG$ int x, int y, int nbCoups
 *       $PRE$
 *            x > 2 && y > 2
 *            nbCoups > 0
 *       $POST$ 
 *            getWidth().equals(y)
 *            getHeight().equals(x)
 *            this.nbCoups == nbCoups
 *            initTable(x, y)
 *            initPointTable(x, y)
 *        
 * @cons
 *     $DESC$ Plateau dont la hauteur <code>x</code>, la largeur 
 *             <code>y</code> et le nombre de coups par partie
 *             entre deux joueurs <code>nbCoups</code> ainsi que la 
 *             liste des joueurs selectionnés, sont définis par 
 *             l'utilisateur.
 *     $ARGS$ int x, int y, int nbCoups, List<StdPlayer> liste
 *     $PRE$
 *             x > 2 && y > 2
 *             nbCoups > 0
 *             liste != null
 *     $POST$
 *            getWidth().equals(y)
 *            getHeight().equals(x)
 *            this.nbCoups == nbCoups
 *            initTable(x, y, liste)
 *            initPointTable(x, y)
 */
public interface Board{
    // CONSTANTES
    
    public final int COOPERATION = 0;
    public final int BETRAYAL = 1;
    
    // REQUETE
    /**
     * retourne le joueur de coordonnée (x,y).
     */
    StdPlayer board(int x, int y);
    
    /**
     * Retourne le plateau de joueurs.
     */
    StdPlayer[][] getBoard();

    /**
     * Retourne la hauteur de la table.
     */
    int getHeight();

    /**
     * Retourne la largeur de la table.
     */
    int getWidth();

    // COMMANDES
    /**
     * Exécute une partie entre les joueurs P1 et P2 de nbCoups coups.
     * @pre 
     *         P1 != null && P2 != null
     *         nbCoups > 0
     */
    void doPlay(StdPlayer P1, StdPlayer P2, int nbCoups);
    
    /**
     * Ajoute les points de partie au tableau de points.
     * Le vainqueur remporte 1 point. Le perdant 0.
     * En cas d'égalité, les deux gagnent 1 point.
     * @pre 
     *         P1 != null && P2 != null
     */
    void addPoints(StdPlayer P1, StdPlayer P2);
    
    /**
     * Réinitialise les joueurs pour pouvoir rejouer ensuite.
     * @pre 
     *         P1 != null && P2 != null
     */
    void resetPlayers(StdPlayer P1, StdPlayer P2);
    
    /**
     * Scanne le plateau et lance les parties.
     */
    void scanning();
    
    /**
     * Initialise le plateau des joueurs
     * @param x et y : dimensions du plateau
     */
    void initTable(int x, int y);
    
    /**
     * Variante de la méthode précédente. Initialise le plateau des joueurs
     * à partir de la liste 'liste'.
     * @param x et y : dimensions du plateau
     *           liste : liste de joueurs
     */
    void initTable(int x, int y, List<StdPlayer> liste);
    
    /**
     * Initialise le plateau des points
     * @param x et y : dimensions du plateau
     * @post 
     *         forall i:[0..x[
     *             forall j:[0..y[ : pointTab[i][j] == 0
     *                 
     */
    void initPointTable(int x, int y);
    
    /**
     * Fait évoluer le plateau pour passer d'une génération n à une génération n + 1;
     * @post
     *         Le joueur qui a perdu la partie prend la couleur de son adversaire
     *         S'il y a égalité, les joueurs gardent leurs couleurs respectives.
     */
    void computePropagation();

    /**
     * Construit la table (élément graphique).
     */
    JPanel printTab();

    /**
     * Ajoute un point au joueur qui est à la case de coordonnée (x,y)
     * @pre 
     *         0 <= x && x <= getHeight() 
     *         0 <= y && y <= getWidth()
     * @post 
     *         Ajoute un point au tableau de points du joueur en coordonnées (x, y)
     */
    void addOnePoint(int x, int y);
    
}
