package Board;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.text.NumberFormatter;

import org.jfree.chart.plot.CategoryPlot;

import Documentation.Help;
import Player.Binary;
import Player.CCCCT;
import Player.GentlePeriodic;
import Player.GiveAndTake;
import Player.Gradual;
import Player.Lunatic;
import Player.Majority;
import Player.Sounder;
import Player.StdPlayer;
import Player.ViciousPeriodic;

public class Assoucomb extends Observable {
    
    // ATTRIBUTS 
    private JFrame mainFrame;
    private StdBoard board;
    private JTabbedPane tabbedPane;
    private JTabbedPane tabbedPane2;
    private JTextArea informations;
    private JButton reset;
    private JButton helpButton;
    
    private JButton basicTest;
    private JButton persoTest;
    private JButton persoTest2;
    private JButton moreOption;
    private JButton evolve;
    
    private JTextField shotsNb;  // nombre de coups par partie
    private JTextField genNb;
    private JTextField row;
    private JTextField col;

    private JTabbedPane picture;
    
    private int genNbInt;    // nombre de génération de base 
    private int currentGen;      // génération courante 
    private JButton suivant;
    
    private Graph graph;
    
    // Correspondance CheckBox / TextField
    private Map<JCheckBox, JTextField> boxToField;
    // Correspondance nom des joueurs / joueurs
    private Map<String, StdPlayer> textToPlayer;
    // Correspondance nom des joueurs / score
    private Map<String, Integer> textToScore;
    
    // CONSTRUCTEUR
    public Assoucomb() {
        createModel();
        createView();
        placeComponents();
        createController();
    }
    
    // COMMANDE
    
    public void display() {
        refresh();
        mainFrame.pack();
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);
    }

    // OUTILS 

     private void createModel() {
           this.genNbInt = 0;
           this.currentGen = 0;
           this.board = null;
     }

     private void createView() {
        mainFrame = new JFrame("S'associer ou se combattre");
        tabbedPane = new JTabbedPane();
        tabbedPane2 = new JTabbedPane();
        informations = new JTextArea(10, 10);
        informations.setEditable(false);
        reset = new JButton("Commencer une nouvelle partie");
        helpButton = new JButton("Afficher l'aide");
        // partie pré-paramétrée
        this.basicTest = new JButton("Lancer le test pré-paramétré (*)");
        
        boxToField = new HashMap<JCheckBox, JTextField>();
        textToPlayer = new HashMap<String, StdPlayer>();
        textToScore = new HashMap<String, Integer>();
        
        // partie personnalisée
        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setMaximumFractionDigits(0);
        NumberFormatter numFormatter = new NumberFormatter(nf);

        this.shotsNb = new JFormattedTextField(numFormatter);
        this.shotsNb.setColumns(3);
        this.genNb = new JFormattedTextField(numFormatter);
        this.genNb.setColumns(3);
        this.row = new JFormattedTextField(numFormatter);
        this.row.setColumns(3);
        this.col = new JFormattedTextField(numFormatter);
        this.col.setColumns(3);
        
        this.persoTest = new JButton("Lancer le test personnalisé");
        this.persoTest2 = new JButton("Lancer le test personnalisé avancé");
        this.moreOption = new JButton("Plus d'options...");

        // affichage
        this.picture = new JTabbedPane(JTabbedPane.TOP,JTabbedPane.SCROLL_TAB_LAYOUT);
        picture.setPreferredSize(new Dimension(400, 400));
        this.evolve = new JButton("Faire évoluer le modèle");
        this.evolve.setEnabled(false);
        
        // Graph
         graph = new Graph("Générations vs Nombre", "Génération", "Nombre de cellules");
        
         // Ajoute toutes les stratégies : crée la checkBox et le jTextField correpondants,
         // et ajoute la relation nom du joueur -> joueur à la map qui s'en charge
        addNewStrat(new Lunatic(15), numFormatter);
        addNewStrat(new GiveAndTake(15), numFormatter);
        addNewStrat(new Binary(15), numFormatter);
        addNewStrat(new GentlePeriodic(15), numFormatter);
        addNewStrat(new ViciousPeriodic(15), numFormatter);
        addNewStrat(new CCCCT(15), numFormatter);
        addNewStrat(new Sounder(15), numFormatter);
        addNewStrat(new Majority(15), numFormatter);
        addNewStrat(new Gradual(15), numFormatter);
        
        suivant = new JButton("Suivant");
    }

    private void placeComponents() {        
        JPanel p = new JPanel(new BorderLayout()); {
            p.add(helpButton, BorderLayout.NORTH);
            
            JPanel q = new JPanel(new BorderLayout()); {
                JPanel r = new JPanel(); {
                    Border b = BorderFactory.createLineBorder(Color.BLACK);
                    Border title = BorderFactory.createTitledBorder(b, "Partie pré-paramétrée",
                            TitledBorder.CENTER, TitledBorder.TOP,
                            new Font("Arial", Font.BOLD, 13), Color.BLACK);
                    r.setBorder(title);
                    
                    JPanel s = new JPanel(new GridLayout(3, 1)); {
                        s.add(basicTest);
                        s.add(new JLabel("(*) Les valeurs par défaut sont :", SwingConstants.CENTER));
                        s.add(new JLabel("20 coups, 8 générations, 8 lignes, 8 colonnes.", SwingConstants.CENTER));
                    }
                    r.add(s);
                }
                q.add(r, BorderLayout.NORTH);
                
                r = new JPanel(); {
                    Border b1 = BorderFactory.createLineBorder(Color.BLACK);
                    Border title1 = BorderFactory.createTitledBorder(b1, "Partie personnalisée",
                            TitledBorder.CENTER, TitledBorder.TOP,
                            new Font("Arial", Font.BOLD, 13), Color.BLACK);
                    r.setBorder(title1);
                    
                    JPanel s = new JPanel(new GridLayout(5, 1)); {
                        JPanel t = new JPanel(); {
                            t.add(new JLabel("Nombre de coups :"));
                            t.add(shotsNb);
                            t.add(new JLabel("Nombre de générations :"));
                            t.add(genNb);
                        }
                        s.add(t);
                        
                        t = new JPanel(); {
                            t.add(new JLabel("Nombre de lignes du plateau (*) :"));
                            t.add(row);
                        }
                        s.add(t);
                        
                        t = new JPanel(); {
                            t.add(new JLabel("Nombre de colonnes du plateau (*) :"));
                            t.add(col);
                        }
                        s.add(t);
                        
                        s.add(new JLabel("(*) Ces valeurs doivent être strictement supérieures à 2.", SwingConstants.CENTER));
                        
                        t = new JPanel(); {
                            t.add(persoTest);
                            t.add(moreOption);
                        }
                        s.add(t);
                    }
                        tabbedPane2.addTab("Basique", s);
                    
                    s = new JPanel(new BorderLayout()); {
                        JPanel t = new JPanel(new GridLayout(0,2)); {
                            Iterator<JCheckBox> it = boxToField.keySet().iterator();
                            while (it.hasNext()) {
                                JCheckBox current = it.next();
                                t.add(current);
                            }
                        }
                        s.add(t, BorderLayout.NORTH);
                        
                        t = new JPanel(new BorderLayout()); {
                            JLabel text = new JLabel("Choisissez 2 stratégies ou plus et cliquez sur \"Suivant\"",
                                    SwingConstants.CENTER);
                            t.add(text, BorderLayout.NORTH);
                            t.add(suivant, BorderLayout.SOUTH);
                        }
                        s.add(t, BorderLayout.SOUTH);
                    }
                    tabbedPane2.addTab("Options", s);
                    r.add(tabbedPane2);
                }
                q.setLayout(new BoxLayout(q, BoxLayout.Y_AXIS));
                q.add(r, BorderLayout.CENTER);
            }
            tabbedPane.addTab("Création", q);
            
            q = new JPanel(new BorderLayout()); {
                JPanel r = new JPanel(new BorderLayout()); {
                    JScrollPane s = new JScrollPane(informations);
                    r.add(s, BorderLayout.CENTER);
                }
                q.add(r, BorderLayout.CENTER);
                q.add(reset, BorderLayout.SOUTH);
            }
            tabbedPane.addTab("Déroulement", q);
        }
        p.add(tabbedPane, BorderLayout.CENTER);
        mainFrame.add(p, BorderLayout.EAST);
        
        JPanel q = new JPanel(new BorderLayout()); {
            q.add(picture, BorderLayout.CENTER);
            q.add(evolve, BorderLayout.SOUTH);
            q.add(this.graph.getPanel(), BorderLayout.NORTH);
        }
        mainFrame.add(q, BorderLayout.CENTER);
     }

     private void createController() {
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        basicTest.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                genNbInt = 8;
                board = new StdBoard();
                picture.addTab("État Initial", board.printTab());
                picture.validate();
                writeInformations();
                refresh();
                updateGraph();
            }
        });

        persoTest.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String valShotsNb = shotsNb.getText();
                String valGenNb = genNb.getText();
                String valRow = row.getText();
                String valCol = col.getText();
                
                if (!valShotsNb.isEmpty() && !valGenNb.isEmpty()
                        && !valRow.isEmpty() && !valCol.isEmpty()) {
                    genNbInt = Integer.parseInt(genNb.getText());
                    board = new StdBoard(Integer.parseInt(valRow),
                            Integer.parseInt(valCol), 
                            Integer.parseInt(valShotsNb));
                    picture.addTab("État Initial", board.printTab());
                    picture.validate();
                    writeInformations();
                    updateGraph();
                    refresh();
                } else {
                    JOptionPane.showMessageDialog(mainFrame, 
                            "Veuillez renseigner tous les paramètres", "Attention", JOptionPane.ERROR_MESSAGE);
                }
                
            }
        });
        
        persoTest2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String valShotsNb = shotsNb.getText();
                String valGenNb = genNb.getText();
                String valRow = row.getText();
                String valCol = col.getText();
                
                if (!valShotsNb.isEmpty() && !valGenNb.isEmpty()
                    && !valRow.isEmpty() && !valCol.isEmpty()) {
                    genNbInt = Integer.parseInt(genNb.getText());
                    
                    int somme = 0;
                    String s;
                    boolean flag = false;
                    
                    Iterator<JCheckBox> it = boxToField.keySet().iterator();
                    while (it.hasNext()) {
                        JCheckBox current = it.next();
                        if (current.isSelected()) {
                            s = boxToField.get(current).getText();
                            if (!s.isEmpty()) {
                                if (!s.isEmpty()) {
                                    somme += Integer.parseInt(s);
                                } else {
                                    flag = true;
                                }
                            }
                        }
                    }
                    if (flag == true) {
                        JOptionPane.showMessageDialog(mainFrame, 
                                "Veuillez remplir tous les champs", "Attention", JOptionPane.ERROR_MESSAGE);
                    } else {
                        int size = Integer.parseInt(valRow) * Integer.parseInt(valCol);
                        if (size < somme) {
                            JOptionPane.showMessageDialog(mainFrame, 
                                    "Vous avez rempli trop de cases (" + (somme - size) + " cases en trop).", "Attention", JOptionPane.ERROR_MESSAGE);
                        } else if (somme < size) {
                            JOptionPane.showMessageDialog(mainFrame, 
                                    "Vous n'avez pas rempli assez de cases (" + (size - somme) + " cases à remplir).", "Attention", JOptionPane.ERROR_MESSAGE);
                        } else {
                            List<StdPlayer> liste = addPlayers();
                            
                            board = new StdBoard(Integer.parseInt(valRow),
                                Integer.parseInt(valCol), 
                                Integer.parseInt(valShotsNb), 
                                liste);
                            
                            picture.addTab("État Initial", board.printTab());
                            picture.validate();
                            writeInformations();
                            updateGraph();
                            refresh();
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(mainFrame, 
                      "Veuillez renseigner tous les paramètres", "Attention", JOptionPane.ERROR_MESSAGE);
                }
                
            }
        });
        
        moreOption.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String valShotsNb = shotsNb.getText();
                String valGenNb = genNb.getText();
                String valRow = row.getText();
                String valCol = col.getText();
                if (!valShotsNb.isEmpty() && !valGenNb.isEmpty()
                        && !valRow.isEmpty() && !valCol.isEmpty()) {
                    tabbedPane2.setSelectedComponent(tabbedPane2.getComponentAt(1));
                    tabbedPane2.setEnabledAt(1, true);
                } else {
                    JOptionPane.showMessageDialog(mainFrame, 
                            "Veuillez renseigner tous les paramètres", "Attention", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        
        helpButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showHelp();
            }
        });
        
        suivant.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int compteur = 0;
                    JPanel p = new JPanel(new BorderLayout()); {
                        JPanel q = new JPanel(new BorderLayout()); {
                            int colInt = Integer.parseInt(col.getText());
                            int rowInt = Integer.parseInt(row.getText());
                            JLabel size = new JLabel("Le plateau fait " + (colInt * rowInt) + " cases ("+ rowInt + " lignes x " + colInt + " colonnes).", SwingConstants.CENTER);
                            JLabel indications = new JLabel("Sélectionnez le nombre de joueurs souhaité par stratégie :", SwingConstants.CENTER);
                            q.add(size, BorderLayout.NORTH);
                            q.add(indications, BorderLayout.SOUTH);
                        }
                        p.add(q, BorderLayout.NORTH);
                        
                        q = new JPanel(new GridLayout(0, 2)); {
                            Iterator<JCheckBox> it = boxToField.keySet().iterator();
                            while (it.hasNext()) {
                                JCheckBox current = it.next();
                                if (current.isSelected()) {
                                    JPanel r = new JPanel(); {
                                        r.add(new JLabel(current.getText()));
                                        r.add(boxToField.get(current));
                                        compteur += 1;
                                    }
                                    q.add(r);
                                }
                            }
                        p.add(q, BorderLayout.CENTER);
                    }
                    
                    p.add(persoTest2, BorderLayout.SOUTH);
                    
                    
                    if (compteur < 2) {
                        JOptionPane.showMessageDialog(mainFrame, 
                            "Il n'y a pas assez de type de joueurs", 
                            "Attention", 
                            JOptionPane.ERROR_MESSAGE
                        );
                    } else {
                        if (tabbedPane2.getTabCount() > 2) {
                            tabbedPane2.removeTabAt(2);
                        }
                        tabbedPane2.addTab("Options (suite)", p);
                        tabbedPane2.setSelectedComponent(tabbedPane2.getComponentAt(2));
                    }
                }
            }
        });

        evolve.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentGen += 1;
                if (board != null && currentGen < genNbInt) {
                    evolve.setEnabled(true);
                } else {
                    evolve.setEnabled(false);
                }
                board.scanning();
                board.computePropagation();
                if (currentGen != genNbInt) {
                    picture.addTab("Génération " + currentGen, board.printTab());
                } else {
                    picture.addTab("État Final", board.printTab());
                }
                picture.setSelectedComponent(picture.getComponentAt(currentGen));
                picture.validate();
                
                writeInformations();
                updateGraph();
            }
        });
        
        reset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                board = null;
                currentGen = 0;
                picture.removeAll();
                graph.getDataset().clear();
                refresh();
            }
        });
    }
    
    private void refresh() {
        if (board != null && currentGen < genNbInt) {
            evolve.setEnabled(true);
        } else {
            evolve.setEnabled(false);
        }
        
        if (board != null) {
            basicTest.setEnabled(false);
            persoTest.setEnabled(false);
            shotsNb.setEnabled(false);
            genNb.setEnabled(false);
            row.setEnabled(false);
            col.setEnabled(false);
            tabbedPane.setSelectedComponent(tabbedPane.getComponentAt(1));
            tabbedPane.setEnabledAt(0, false);
            tabbedPane.setEnabledAt(1, true);
            tabbedPane2.setSelectedComponent(tabbedPane2.getComponentAt(0));
            tabbedPane2.setEnabledAt(0, false);
            tabbedPane2.setEnabledAt(1, false);
        } else {
            basicTest.setEnabled(true);
            persoTest.setEnabled(true);
            shotsNb.setEnabled(true);
            genNb.setEnabled(true);
            row.setEnabled(true);
            col.setEnabled(true);
            tabbedPane.setSelectedComponent(tabbedPane.getComponentAt(0));
            tabbedPane.setEnabledAt(0, true);
            tabbedPane.setEnabledAt(1, false);
            tabbedPane2.setEnabledAt(0, true);
            tabbedPane2.setEnabledAt(1, false);
            informations.setText("");
            if (tabbedPane2.getTabCount() > 2) {
                tabbedPane2.removeTabAt(2);
            }
        }
    }
    
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new Assoucomb().display();
            }
        });
    }
    
    // OUTILS
    
    /**
     * Ajoute une nouvelle stratégie. Crée la checkBox correspondante, l'ajoute
     * à la map box -> textField, puis ajoute le nom du joueur et le joueur à la map
     * nom du joueur -> joueur 
     */
    private void addNewStrat(StdPlayer p, NumberFormatter nf) {
        JCheckBox jc = new JCheckBox(p.getName());
        JTextField newStratNb = new JTextField();
        newStratNb.setColumns(3);
        boxToField.put(jc, newStratNb);
        textToPlayer.put(p.getName(), p);
    }
    
    void writeInformations() {
        if (currentGen == 0) {
            informations.append("État Initial :\n");
            writeBoard();
            informations.append("\n");
        } else {
            if (currentGen != genNbInt) {
                informations.append("Génération " + currentGen);
            } else {
                informations.append("État Final");
            }
            informations.append(" :\n");
            writeBoard();
            informations.append("\n");
            
            if (currentGen == genNbInt) {
                Collection<Integer> scores = textToScore.values();
                Integer max = Collections.max(scores);
                int maxNb = Collections.frequency(scores, max);
                if (maxNb > 1) {
                    informations.append("Et les gagnants sont : (roulement de tambour dans la salle...)");
                } else {
                    informations.append("Et le gagnant est : (roulement de tambour dans la salle...)");
                }
                informations.append("\n");
                
                Iterator<String> it = textToScore.keySet().iterator();
                while (it.hasNext()) {
                    String current = it.next();
                    Integer nb = textToScore.get(current); 
                    if (nb == max) {
                        informations.append("- " + current + "\n");
                    }
                }
            }
        }
    }
    
    private void showHelp() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Help().display();
            }
        });
    }
    
    void writeBoard() {
        StdPlayer[][] tab = board.getBoard();
        double total = board.getHeight() * board.getWidth();
        textToScore.clear();
        for (int i = 0; i < board.getHeight(); i++) {
            for (int j = 0; j < board.getWidth(); j++) {
                String name = tab[i][j].getName();
                Integer number = textToScore.get(name);
                if (number != null) {
                    int formerScore = textToScore.get(name);
                    textToScore.put(name, formerScore + 1);
                } else {
                    textToScore.put(name, 1);
                }
            }
        }
        
        Iterator<String> it = textToScore.keySet().iterator();
        while (it.hasNext()) {
            String current = it.next();
            Integer nb = textToScore.get(current); 
            if (nb != 0) {
                informations.append("" + nb + " - " + current.toLowerCase());
                if (nb == 1) {
                    informations.append("s");
                }
                informations.append(" - " + (double) Math.round(10 * (nb/total * 100)) / 10 + "%\n");
            }
        }
    }
    
    private List<StdPlayer> addPlayers() {
        List<StdPlayer> list = new ArrayList<StdPlayer>(); 
        int shot = Integer.parseInt(shotsNb.getText());
        
        Iterator<JCheckBox> it = boxToField.keySet().iterator();
        while (it.hasNext()) {
            JCheckBox current = it.next();
            JTextField field = boxToField.get(current);
            if (current.isSelected() && Integer.parseInt(field.getText()) > 0) {
                for (int i = 0 ; i < Integer.parseInt(field.getText()) ; ++i) {
                    StdPlayer player = (StdPlayer)textToPlayer.get(current.getText()).clone();
                    player.setShotsNb(shot);
                    list.add(player);
                }
            }
        }
        return list;
    }
     
     private void updateGraph() {
         Iterator<String> it = textToScore.keySet().iterator();
            while (it.hasNext()) {
                String current = it.next();
                Integer nb = textToScore.get(current); 
                CategoryPlot plot = (CategoryPlot) graph.getLineChart().getPlot();
                StdPlayer player = textToPlayer.get(current);
                int number = graph.getDataset().getRowIndex(current);
                if (player != null && number > -1) {
                    plot.getRenderer().setSeriesPaint(number, player.getColor());
                }    
                graph.changeData(nb, current, currentGen);
            }
     }
}
