package Board;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.Collections;
import java.util.List;
import java.util.Observable;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;

import Player.Binary;
import Player.CCCCT;
import Player.GentlePeriodic;
import Player.GiveAndTake;
import Player.Gradual;
import Player.Lunatic;
import Player.Majority;
import Player.Sounder;
import Player.StdPlayer;
import Player.ViciousPeriodic;

public class StdBoard extends Observable implements Board {
    // ATTRIBUTS
    private int nbCoups;

    private int width;
    private int height;
    
    private StdPlayer[][] tab;
    private int[][] pointTab;
    
    private StdPlayer[][] newTab;
    
    // CONSTRUCTEURS
    public StdBoard() {
        this(8, 8, 20);
    }
    
    public StdBoard(int x, int y, int nbCoups) {
        if (x <= 2 || y <= 2) {
            throw new AssertionError();
        }
        if (nbCoups <= 0) {
            throw new AssertionError();
        }
        
        this.width = y;
        this.height = x;
        this.nbCoups = nbCoups;
        initTable(x, y);
        initPointTable(x, y);
        newTab = new StdPlayer[x][y];
    }
    
    public StdBoard(int x, int y, int nbCoups, List<StdPlayer> liste) {
        if (x <= 2 || y <= 2) {
            throw new AssertionError();
        }
        if (nbCoups <= 0) {
            throw new AssertionError();
        }

        this.width = y;
        this.height = x;
        this.nbCoups = nbCoups;
        initTable(x, y, liste);
        initPointTable(x, y);
        newTab = new StdPlayer[x][y];
    }
    
    // REQUETES
    public StdPlayer board(int x, int y) {
        return tab[x][y];
    }
    
    public StdPlayer[][] getBoard() {
        return tab;
    }
    
    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }
    
    // COMMANDES
    @Override
    public void doPlay(StdPlayer P1, StdPlayer P2, int nbCoups) {
        P1.setEnemy(P2);
        P2.setEnemy(P1);
        
        for (int i = 0 ; i < nbCoups ; ++i) {
            P1.strategicShot();
            P2.strategicShot();
        }
        
        addPoints(P1, P2);
        resetPlayers(P1, P2);
    }

    @Override
    public void addPoints(StdPlayer P1, StdPlayer P2) {
        if (P1.getShotNb() != P2.getShotNb()) {
            throw new AssertionError();
        }
        
        int nbP1 = 0;
        int nbP2 = 0;
        int[] tabP1 = P1.getShots() ;
        int[] tabP2 = P2.getShots() ;
        for (int i = 0 ; i < P1.getShotNb() - 2 ; i++) {
            if (tabP1[i] == COOPERATION && tabP2[i] == COOPERATION) {
                nbP1 += 1;
                nbP2 += 1;
            } else if (tabP1[i] == COOPERATION && tabP2[i] == BETRAYAL) {
                nbP1 += 5;
            } else if (tabP1[i] == BETRAYAL && tabP2[i] == COOPERATION) {
                nbP2 += 5;
            } else {
                nbP1 += 3;
                nbP2 += 3;
            }
        }
        if (nbP1 > nbP2) {
            addOnePoint(P2.getCooX(), P2.getCooY());
        } else if (nbP1 == nbP2) {
            addOnePoint(P1.getCooX(), P1.getCooY());
            addOnePoint(P2.getCooX(), P2.getCooY());
        } else {
            addOnePoint(P1.getCooX(), P1.getCooY());
        }
    }

    @Override
    public void resetPlayers(StdPlayer P1, StdPlayer P2) {
        P1.reset();
        P2.reset();
    }

    @Override
    public void scanning() {
        for (int i = 0 ; i < height ; i++) {
            for (int j = 0 ; j < width ; j++) {
                    if (i == 0) { // on est dans la première ligne
                        // jouer avec le voisin du dessus
                        doPlay(tab[i][j], tab[height - 1][j], nbCoups);
                        // jouer avec le voisin de droite
                        if (j == width - 1) { // on est dans la dernière colonne
                            doPlay(tab[i][j], tab[i][0], nbCoups);
                        } else { // on est dans une colonne précédente
                            doPlay(tab[i][j], tab[i][j + 1], nbCoups);
                        }
                    } else if (j == width - 1) { // on est dans la dernière colonne
                        // jouer avec le voisin du dessus
                        doPlay(tab[i][j], tab[i - 1][j], nbCoups);
                        // jouer avec le voisin de droite
                        doPlay(tab[i][j], tab[i][0], nbCoups);
                    } else { // on est n'importe où sauf dans la première colonne/ligne
                        // jouer avec le voisin du dessus
                        doPlay(tab[i][j], tab[i - 1][j], nbCoups);
                        // jouer avec le voisin de droite
                        doPlay(tab[i][j], tab[i][j + 1], nbCoups);
                    }
            }
        }
    }

    @Override
    public void initTable(int x, int y) {
        tab = new StdPlayer[x][y];
        for (int i = 0 ; i < x ; i++) {
            for (int j = 0 ; j < y ; j++) {
                tab[i][j] = alea();
                tab[i][j].setCoo(i, j);
            }
        }
    }
    
    public void initTable(int x, int y, List<StdPlayer> liste) {
        Collections.shuffle(liste);
        tab = new StdPlayer[x][y];
        int n = 0;
        for (int i = 0 ; i < x ; i++) {
            for (int j = 0 ; j < y ; j++) {
                tab[i][j] = liste.get(n);
                tab[i][j].setCoo(i, j);
                n = n + 1;
            }
        }
    }
    
    public void initNewTable(StdPlayer newPlayer, 
            int oldPlayerX, int oldPlayerY) {
        newTab[oldPlayerX][oldPlayerY] = (StdPlayer) newPlayer.clone();
        newTab[oldPlayerX][oldPlayerY].setCoo(oldPlayerX, oldPlayerY);
    }

    @Override
    public void initPointTable(int x, int y) {
        pointTab = new int[x][y];
        
        for (int i = 0 ; i < x ; i++) {
            for (int j = 0 ; j < y ; j++) {
                pointTab[i][j] = 0;
            }
        }
    }

    @Override
    public void computePropagation() {
        for (int i = 0 ; i < height ; i++) {
                for (int j = 0 ; j < width ; j++) {
                    if (i == 0) { // dans la première ligne du tableau
                        if (j == 0) { // dans la toute première case du tableau
                            maxPointPlayer(i,j, i+1,j, i,j+1, height-1,j, i,width-1);
                        } else if (j == width - 1) { // dans la dernière case de la première ligne 
                            maxPointPlayer(i,j, i+1,j, i,0, height-1,j, i,j-1);
                        } else { // toutes les cases entre la première et la dernière de la première ligne
                            maxPointPlayer(i,j, i+1,j, i,j+1, height-1,j, i,j-1);
                        }
                    } else if (i == height - 1) { // pour la dernière ligne du tableau
                        if (j == 0) { // dans la première case de la dernière ligne du tableau
                            maxPointPlayer(i,j, 0,j, i,j+1, i-1,j, i,width-1);
                        } else if (j == width - 1) { // dans la toute dernière case du tableau
                            maxPointPlayer(i,j, 0,j, i,0, i-1,j, i,j-1);
                        } else { // toutes les cases entre la première et la dernière de la dernière ligne
                            maxPointPlayer(i,j, 0,j, i,j+1, i-1,j, i,j-1);
                        }
                    } else if(j == width - 1) { // dans la dernière colonne
                        maxPointPlayer(i,j, i+1,j, i,0, i-1,j, i,j-1);
                    } else if (j == 0) { // dans la première colonne
                        maxPointPlayer(i,j, i+1,j, i,j+1, i-1,j, i,width-1);
                    } else { // toutes les cases au centre du tableau
                        maxPointPlayer(i,j, i+1,j, i,j+1, i-1,j, i,j-1);
                    }
                }
            }
        
        // on enregistre newTab comme nouveau plateau de jeu
        for (int i = 0; i < getHeight(); ++i) {
                for (int j = 0; j < getWidth(); ++j) {
                    tab[i][j] = newTab[i][j];
                }
        }
    }

    @Override
    public JPanel printTab() {
        StdPlayer[][]table = this.getBoard();
        JPanel p = new JPanel(new GridLayout(getHeight(), getWidth()));
        Border b = BorderFactory.createLineBorder(Color.BLACK, 1);
        for (int i = 0; i < getHeight(); ++i) {
            for (int j = 0; j < getWidth(); ++j) {
                Color c = table[i][j].getColor();
                JPanel q = new JPanel();
                q.setBackground(c);
                q.setBorder(b);
                q.setToolTipText("" + table[i][j].getName());
                p.add(q);
            }
        }
        return p;
    }
    
    @Override
    public void addOnePoint(int x, int y) {
        pointTab[x][y] += 1;
    }
    
    // OUTIL
    /**
     * Methode qui place les joueurs aléatoirement sur le plateau de jeu
     */
    private StdPlayer alea() {
        int rand = (int) (Math.random() * (10 - 1));
        
        if (rand < 1) {
            return new Lunatic(nbCoups);
        } else if (rand < 2) {
            return new GiveAndTake(nbCoups);
        } else if (rand < 3) {
            return new Binary(nbCoups);
        } else if (rand < 4) {
            return new GentlePeriodic(nbCoups);
        } else if (rand < 5) {
            return new ViciousPeriodic(nbCoups);
        } else if (rand < 6) {
            return new CCCCT(nbCoups);
        } else if (rand < 7) {
            return new Sounder(nbCoups);
        } else if (rand < 8) {
            return new Majority(nbCoups);
        } else {
            return new Gradual(nbCoups);
        }
    }
    
    /**
     * Methode outil qui calcule le joueur avec le nombre de points
     * max et le stocke 
     */
    private void maxPointPlayer(int x,int y, int x1,int y1,
            int x2,int y2, int x3,int y3, int x4,int y4) {
        int[][] points = new int[][] {{this.pointTab[x][y], x, y}, 
            {this.pointTab[x1][y1], x1, y1}, 
            {this.pointTab[x2][y2], x2, y2}, 
            {this.pointTab[x3][y3], x3, y3}, 
            {this.pointTab[x4][y4], x4, y4}}; 
        int max = 0;
        int xMax = 0;
        int yMax = 0;
        int nMax = 1;
        for (int i = 0 ; i < 5 ; ++i) {
            if (max < points[i][0]) {
                max = Math.max(max, points[i][0]);
                xMax = points[i][1];
                yMax = points[i][2];
                nMax = 1;
            } else if (max == points[i][0]) {
                nMax = nMax + 1;
            }
        }
        
        if (nMax >= 2) {
            initNewTable(this.tab[x][y], x, y);
        } else {
            initNewTable(this.tab[xMax][yMax], x, y);
        }
        // TODO
        /*System.out.println("Case : " + points[i][1] + ":" + points[i][2]);
        System.out.println("Nombre de points : "+ points[i][0]);
        System.out.println(i + "--> Max = " + max);*/
    }
}
