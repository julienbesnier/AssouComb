package Documentation;

import java.awt.Dimension;
import java.io.IOException;
import java.net.URL;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

public class Help {
    // CONSTANTES
    private int SIZE = 800;
    
    // ATTRIBUTS
    private JFrame mainframe;
    private JEditorPane page;
    private JScrollPane lift;
    private URL model;
    
    
    // CONSTRUCTEUR 
    public Help() {
        createModel();
        createView();
        placeComponents();
    }
    
    // PARTIE GRAPHIQUE
    public void display() {
        mainframe.pack();
        mainframe.setLocationRelativeTo(null);
        mainframe.setVisible(true);
    }
    
    private void createModel() {
        model = getClass().getResource("/help.html");
    }
    
    private void createView() {
        mainframe = new JFrame("Module d'aide");
        mainframe.setTitle("");
        page = new JEditorPane();
        page.setEditable(false);
        page.setPreferredSize(new Dimension(SIZE, SIZE));
        lift = new JScrollPane(page);
        // frame.getContentPane().add(lift);
        // lift.setViewportView(page);
    }
    
    private void placeComponents() {
            try {
                page.setPage(model);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            mainframe.add(lift);
    }
    
    // MAIN
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Help().display();
            }
        });
    }
}
